(function () {
    'use strict';

    function main($resource) {

        return {
            issues: $resource(
                'https://gitlab.com/api/v4/issues?per_page=100',
                {},
                {
                    query: {
                        method: 'GET',
                        isArray: true
                    }
                }
            ),
            issues_time_stats: $resource(
                'https://gitlab.com/api/v4/projects/:id/issues/:issue_iid/time_stats?per_page=100',
                {},
                {
                    get: {
                        method: 'GET'
                    }
                }
            ),
            projects: $resource(
                'https://gitlab.com/api/v4/projects?per_page=100',
                {},
                {
                    query: {
                        method: 'GET',
                        isArray: true
                    }
                }
            ),
            projects_issues: $resource(
                'https://gitlab.com/api/v4/projects/:id/issues?per_page=100',
                {},
                {
                    query: {
                        method: 'GET',
                        isArray: true
                    }
                }
            ),
            projects_labels: $resource(
                'https://gitlab.com/api/v4/projects/:id/labels?per_page=100',
                {},
                {
                    query: {
                        method: 'GET',
                        isArray: true
                    }
                }
            ),
            projects_milestones: $resource(
                'https://gitlab.com/api/v4/projects/:id/milestones?per_page=100',
                {},
                {
                    query: {
                        method: 'GET',
                        isArray: true
                    }
                }
            )
        };
    }

    angular.module('GitLabReportApp').factory('gitlab', main);

})();

