(function () {
    'use strict';

    function main($rootScope, $scope, gitlab, auth) {

        $rootScope.$on('local-storage-updated', function () {
            $scope.filter_milestone = localStorage.getItem('milestone');
            $scope.loadIssues();
        });

        $scope.loadIssues = function () {
            var params = {};
            if (localStorage.getItem('access_token')) {
                params.access_token = localStorage.getItem('access_token');
            } else {
                return;
            }
            if (localStorage.getItem('project_id')) {
                params.id = localStorage.getItem('project_id');
            } else {
                return;
            }
            if (localStorage.getItem('state')) {
                params.state = localStorage.getItem('state');
            }
            if (localStorage.getItem('milestone')) {
                params.milestone = localStorage.getItem('milestone');
            }
            if (localStorage.getItem('labels')) {
                params.labels = localStorage.getItem('labels');
            }
            $scope.issues = gitlab.projects_issues.query(
                params,
                function () {
                    $scope.recalculateTimeStats();
                },
                function () {
                    auth.redirectToOauth();
                }
            );
        };

        $scope.recalculateTimeStats = function () {
            $scope.comulative_time_spent = 0;
            $scope.comulative_time_estimate = 0;
            angular.forEach($scope.issues, function (issue) {
                $scope.comulative_time_estimate += issue.time_stats.time_estimate;
                $scope.comulative_time_spent += issue.time_stats.total_time_spent;
            });
        };

    }

    angular.module('GitLabReportApp').controller('TableController', main);

})();

